#!/bin/env bash

# Copyright 2019 high_octane
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

####################
# The build script #
####################

# I use msys2's environment when I develop software on Windows, so this will
# probably fail to build on systems that aren't UNIX-like. I don't plan on
# adding support for Microsoft's compiler, but it might happen some day. I've
# only tested compilation with g++.

test_define=""
dbg_cmd="-g3 -DCHEXT_DEBUG"
opt_cmd="-O3 -s"
config="$dbg_cmd"
compiler="g++"
input="chext"
output="chext"

[ ! -z "$CXX" ] && { compiler="$CXX" ;}

usage="Usage: build.sh [OPTION]...

Options:
  --config                 Can either be \"debug\" or \"release\". Defaults to
                             \"Debug\". \"Debug\" will define the macro
                             \"CHEXT_DEBUG\" and add debugging symbols.
                             \"Release\" will optimize and strip the executable.
  --test                   Set to the source file you want to test. This will
                             define a macro that allows the source file to be
                             used stand-alone. Do not include the extension of
                             the source file.
  -h, --help               Display this information.

The default compiler \"$compiler\" can be overridden by using the environment variable
CXX. CXX is currently set to \"$CXX\".\n"

error_junk()
{
    local junk="$1"

    echo -ne >&2 "[Error] Junk \"$junk\" was passed in as an argument.\n"
}

error_optarg()
{
    local opt="$1"
    local arg="$2"

    echo -ne >&2 "[Error] Invalid argument \"$arg\" passed to option \"$opt\".\n"
}

show_usage()
{
    echo -ne >&2 "$usage"
    exit 1
}

handle_arg_config()
{
    local arg="$1"

    case "$arg" in
        debug)   config="$dbg_cmd";;
        release) config="$opt_cmd";;
        *)       error_optarg "--config" "$arg"; show_usage;;
    esac
}

handle_arg_test()
{
    local test_file="$1"

    test_define="-DCHEXT_TEST_${test_file^^}"
    input="$test_file"
    output="$test_file"
}

args=( "$@" )
for ((i = 0; i < ${#args[@]}; i += 2));
do
    case "${args[$i]}" in
        --config)    handle_arg_config "${args[$i+1]}";;
        --test)      handle_arg_test   "${args[$i+1]}";;
        -h | --help) show_usage;;
        *)           error_junk "${args[$i]}"; show_usage;;
    esac
done

[ ! -d bin ] && { mkdir bin ;}

"$compiler" \
    -Wall \
    -Wextra \
    -Wno-literal-suffix\
    $config \
    -D__USE_MINGW_ANSI_STDIO \
    -D_FILE_OFFSET_BITS=64 \
    $test_define \
    "src/$input.c*" \
    `pkg-config --cflags --libs opencv4` \
    -o"bin/$output"
