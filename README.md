# CH Interactive Metadata Extractor
### Build Dependencies
- OpenCV (>= 4.0)
- pkg-config
- Bash
- g++ (or compatible compiler)
- GNU coreutils
### Build Instructions
Call "build.sh" and, granted that you have the necessary dependencies, it will write a binary to "bin". You may wish to call "build.sh" with "-h" or "--help" to learn about available build options.
##### Windows
If you plan on building this project for Windows, you will need to use a UNIX-like environment like [MSYS2](https://www.msys2.org). It might be possible to add Microsoft compiler support later on, I'm just very unfamiliar with it. As long as solution files are not involved, I'll be satisfied.
### Contributing
Please do! All contributions are greatly appreciated.
##### Programming Style
The one golden rule to follow: No tab characters, please! Other than that, I'm pretty lax. In the interest of maintaining some sort of consistency, try to format your code like so:
```c
#include <stdio.h>

#define ITER_SIZE 100 // Preprocessor macros are all-caps and underscore separated.

typedef struct // I prefer typedefing structs.
{
    int a[ITER_SIZE];
    int b[ITER_SIZE];
    int c[ITER_SIZE];
} StructName; // No underscore for structs and all words are capitalized.

int main(int argc, char **argv)
{
    int var_name = 0; // Use underscores for variables and function names.
    StructName foo = {};
    for (int i = 0; i < ITER_SIZE; ++i)
    {
        foo.a[i] = i;
        foo.b[i] = i + 10;
        foo.c[i] = i + 20;
    }

    var_name = foo.a[5] + foo.b[10] + foo.c[15];
    printf("var_name: %d\n", var_name);
    return 0;
}
```
I also prefer [unity builds](https://en.wikipedia.org/wiki/Single_Compilation_Unit), so include external C/C++ files in the main compilation unit, "src/chext.cpp". Header files are completely optional.

Due to my ignorance, try not to contribute code that's written in a super modern C++ dialect or I'll have trouble comprehending it.
### Running the Program
##### Technical Issues
Heads up, **the GUI \<X\> button is a lie!** Due to technical shortcomings, the window cannot be closed in the traditional way, so you must either press \<q\> or, if the commandline has focus, \<Ctrl-C\>. A proper fix will be implemented eventually, but it will, unfortunately, be platform specific.
##### Examples
On the commandline (Bash):
```shell
bin/chext /path_to_video/input.mp4
```
If you are using a GUI file browser, you may also drap-and-drop a video file on to the program.
##### Compile and Run
Here's a quick one-liner to compile and run the program, but only if compilation is successful:
```shell
./build.sh && bin/chext /path_to_video/input.mp4
```
### Developer Discussion
If you are interesting in following the discussion about the development of chext, then please check out the [official Riot chatroom](https://riot.im/app/#/room/#chext:matrix.org)! It is open to all, so take a gander if you'd like.
