/*
 * Copyright 2019 high_octane
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef CHEXT_TEST_GET_MEDIAINFO
#include <assert.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "serialize.h"
#include "mp4_minspec.h"
#endif

/* To prevent hair-ripping bugs: */
#define ftell ftello
#define fseek fseeko

/* Ignore this load of bollocks. */
#if 0
typedef struct
{
    char **lvl0;
    char **lvl1;
    char **lvl2;
    char **lvl3;
    char **lvl4;
    char **lvl5;
} GmiGenericVipHierarchy

GmiGenericVipHierarchy gmi_mp4_vip_boxes =
{
    .lvl0 = {/*"ftyp",*/ "moov"},
    .lvl1 = {"mvhd", "trak"},
    .lvl2 = {"mdia"},
    .lvl3 = {"hdlr", "minf"},
    .lvl4 = {"stbl"},
    .lvl5 = {"stsd"}
};

enum GmiMp4Lvl0Tags {GMI_MP4_MOOV};
enum GmiMp4Lvl1Tags {GMI_MP4_MVHD, GMI_MP4_TRAK};
enum GmiMp4Lvl2Tags {GMI_MP4_MDIA};
enum GmiMp4Lvl3Tags {GMI_MP4_HDLR, GMI_MP4_MINF};
enum GmiMp4Lvl4Tags {GMI_MP4_STBL};
enum GmiMp4Lvl5Tags {GMI_MP4_STSD};
#endif

enum GmiFormatTag {GMI_FTAG_MP4};
enum GmiTimeType  {GMI_TTYPE_CREATION, GMI_TTYPE_MODIFICATION};
enum GmiResType   {GMI_RTYPE_WIDTH, GMI_RTYPE_HEIGHT};
enum GmiEncType   {GMI_ETYPE_VIDEO, GMI_ETYPE_AUDIO};
enum GmiFpsType   {GMI_FTYPE_NUMERATOR, GMI_FTYPE_DENOMINATOR};
enum GmiDurType   {GMI_DTYPE_TIMEBASE, GMI_DTYPE_DURATION};
enum GmiRemainingFlags
{
    GMI_RFLAG_FILESIZE          = 1 << 0,
    GMI_RFLAG_CONTAINER         = 1 << 1,
    GMI_RFLAG_TIME_CREATION     = 1 << 2,
    GMI_RFLAG_TIME_MODIFICATION = 1 << 3,
    GMI_RFLAG_DUR_TIMEBASE      = 1 << 4,
    GMI_RFLAG_DUR_DURATION      = 1 << 5,
    GMI_RFLAG_FPS_NUM           = 1 << 6,
    GMI_RFLAG_FPS_DEN           = 1 << 7,
    GMI_RFLAG_RES_WIDTH         = 1 << 8,
    GMI_RFLAG_RES_HEIGHT        = 1 << 9,
    GMI_RFLAG_ENC_VIDEO         = 1 << 10,
    GMI_RFLAG_ENC_AUDIO         = 1 << 11,
    GMI_RFLAG_ALL               = 4095
};

static const char *wrnmsg_ts[] =
{
    {
        "[Warning] Creation timestamp information is absent. This is a common\n"
        "          occurance in certain container formats, and in videos uploaded to\n"
        "          tube sites. To rectify this, you will need to either provide this\n"
        "          information manually or run a program to retrieve it for you.\n\n"
    },
    {
        "[Warning] Modification timestamp information is absent. This usually\n"
        "          contains the time when the render was completed. This timestamp\n"
        "          is the least important of the two, so just leave it blank.\n\n"
    }
};

/*
 * Generic struct which will hold the mediainfo from any format to be written
 * out to a file (or stream). The metadata gathered by ML algorithms will be
 * stored elsewhere.
 */
typedef struct
{
    int remaining;

    /*
     * RawInfo is meant for internal use only. Do not use string functions with
     * these char arrays, for they are not null-terminated! These will also be
     * used as the raw data output for CSV/JSON/whatever format.
     */
    struct RawInfo
    {
        size_t filesize;
        char   container[4]; /* The way this is used now is null-terminated. */
        struct Time
        {
            size_t creation;
            size_t modification;
        } time;
        struct Duration
        {
            uint32_t timebase;
            size_t   duration;
        } dur;
        struct Framerate
        {
            uint32_t num;
            uint32_t den;
        } fps;
        struct Resolution
        {
            uint16_t width;
            uint16_t height;
        } res;
        struct Encoders
        {
            char video[4];
            char audio[4];
        } enc;
    } raw;

    /*
     * These are meant to be displayed to the terminal at some point.
     */
    struct PrettyInfo
    {
        char filesize[6];   /* 3.64G 1021M */
        char container[31]; /* ISO Base Media mp42 */
        struct TimeType
        {
            char creation[21];     /* 2019-04-13T03:21:56Z */
            char modification[21]; /* 2018-06-21T12:03:14Z */
        } time;
        struct Duration
        {
            char hms[10];
        } dur;
        struct Framerate
        {
            float computed; /* 29.97 30 59.94 */
        } fps;
        struct Resolution
        {
            char std[4]; /* SD HD FHD UHD 4K */
        } res;
        struct Encoders
        {
            char video[10];
            char audio[10];
        } enc;
    } pretty;
} GmiMediaInfo;

GmiMediaInfo mi = {.remaining = GMI_RFLAG_ALL, {}, {}};

/* Remembers the position of every box/fullbox we come across. */
static fpos_t   gmi_box_fpos = 0;

/* Remembers the position of a single trak box for if we found the wrong one. */
static fpos_t   gmi_trak_fpos = 0;

static fpos_t   gmi_mdhd_fpos = 0;

/* If we find a bad trak, it's time to go back! */
static bool     gmi_bad_trak = false;

/* If we successfully parse a trak in its entirety, we increment. */
static unsigned gmi_trak_success = 0;

void get_raw_filesize(size_t filesize, FILE *fp)
{
    fseeko(fp, 0, SEEK_END);
    filesize = ftello(fp);
    fseeko(fp, 0, SEEK_SET);

    mi.raw.filesize = filesize;

    mi.remaining ^= GMI_RFLAG_FILESIZE;
}

static void get_raw_container(uint32_t container, enum GmiFormatTag ftag)
{
    switch (ftag)
    {
        case GMI_FTAG_MP4:
        {
            if (container == 0x71742020 /* qt   */)
            {
                strcpy(mi.raw.container, "mov");
            }
            else
            {
                strcpy(mi.raw.container, "mp4");
            }
            mi.remaining ^= GMI_RFLAG_CONTAINER;
            break;
        }
    }
}

static inline bool good_timestamp(size_t timestamp, enum GmiTimeType ttype)
{
    if (!timestamp)
    {
        fputs(wrnmsg_ts[ttype], stderr);
        return false;
    }

    return true;
}

static void get_raw_resolution(size_t res, enum GmiResType rtype)
{
    switch (rtype)
    {
        case GMI_RTYPE_WIDTH:
        {
            mi.raw.res.width = res;
            mi.remaining ^= GMI_RFLAG_RES_WIDTH;
            break;
        }
        case GMI_RTYPE_HEIGHT:
        {
            mi.raw.res.height = res;
            mi.remaining ^= GMI_RFLAG_RES_HEIGHT;
            break;
        }
        default:
        {
            fprintf(stderr, "[Error] Invalid ResType specified in function: %s\n", __FUNCTION__);
            exit(1);
        }
    }
}

static void get_raw_timestamp(size_t timestamp,
                              enum GmiTimeType ttype,
                              enum GmiFormatTag ftag)
{
    /*
     * Convert from whatever to "UNIX" timestamp format. The difference between
     * actual UNIX format and our format is the use of a 64-bit unsigned number
     * as the result. I don't anticipate any of the videos we scrape metadata
     * from being made before 1970...
     */

    /* UNIX Epoch: In seconds since midnight, Jan. 1, 1970, in UTC time. */

    size_t difference = 0;

    if (good_timestamp(timestamp, ttype))
    {
        switch (ftag)
        {
            case GMI_FTAG_MP4:
            {
                /* In seconds since midnight, Jan. 1, 1904, in UTC time. */
                difference = 2082844800;
                timestamp -= difference;
                break;
            };
            default:
            {
                fprintf(stderr, "[Error] Invalid FormatTag specified in function: %s\n", __FUNCTION__);
                exit(1);
            }
        }
    }

    switch (ttype)
    {
        case GMI_TTYPE_CREATION:
        {
            mi.raw.time.creation = timestamp;
            mi.remaining ^= GMI_RFLAG_TIME_CREATION;
            break;
        }
        case GMI_TTYPE_MODIFICATION:
        {
            mi.raw.time.modification = timestamp;
            mi.remaining ^= GMI_RFLAG_TIME_MODIFICATION;
            break;
        }
        default:
        {
            fprintf(stderr, "[Error] Invalid TimeType specified in function: %s\n", __FUNCTION__);
            exit(1);
        }
    }
}

static inline void get_raw_duration(size_t dur_seg, enum GmiDurType dtype)
{
    switch(dtype)
    {
        case GMI_DTYPE_TIMEBASE:
        {
            mi.raw.dur.timebase = dur_seg;
            mi.remaining ^= GMI_RFLAG_DUR_TIMEBASE;
            break;
        }
        case GMI_DTYPE_DURATION:
        {
            mi.raw.dur.duration = dur_seg;
            mi.remaining ^= GMI_RFLAG_DUR_DURATION;
            break;
        }
        default:
        {
            fprintf(stderr, "[Error] Invalid DurType specified in function: %s\n", __FUNCTION__);
            exit(1);
        }
    }
}

static inline void get_raw_framerate(uint32_t fps_seg, enum GmiFpsType ftype)
{
    switch(ftype)
    {
        case GMI_FTYPE_NUMERATOR:
        {
            mi.raw.fps.num = fps_seg;
            mi.remaining ^= GMI_RFLAG_FPS_NUM;
            break;
        }
        case GMI_FTYPE_DENOMINATOR:
        {
            mi.raw.fps.den = fps_seg;
            mi.remaining ^= GMI_RFLAG_FPS_DEN;
            break;
        }
        default:
        {
            fprintf(stderr, "[Error] Invalid FpsType specified in function: %s\n", __FUNCTION__);
            exit(1);
        }
    }
}

static inline void get_raw_encoder(uint32_t enc, enum GmiEncType etype)
{
    switch(etype)
    {
        case GMI_ETYPE_VIDEO:
        {
            memcpy(mi.raw.enc.video, &enc, 4);
            mi.remaining ^= GMI_RFLAG_ENC_VIDEO;
            break;
        }
        case GMI_ETYPE_AUDIO:
        {
            memcpy(mi.raw.enc.audio, &enc, 4);
            mi.remaining ^= GMI_RFLAG_ENC_AUDIO;
            break;
        }
        default:
        {
            fprintf(stderr, "[Error] Invalid EncType specified in function: %s\n", __FUNCTION__);
            exit(1);
        }
    }
}

static bool mp4_bad_trak(uint32_t handler_type)
{
    enum Mp4HdlrType {VIDE = 0x76696465, SOUN = 0x736f756e};

    if (!(handler_type == VIDE || handler_type == SOUN))
    {
        /*
         * HHHOOOLLLYYY SMOKES! Abort mission! We literally went down the wrong
         * track. Time to move up the stack and find the next trak.
         */
        gmi_bad_trak = true;
        return true;
    }

    return false;
}

static Mp4Box mp4_parse_b(Mp4Box b, FILE *fp)
{
    memset(&b, 0, sizeof(Mp4Box));

    fgetpos(fp, &gmi_box_fpos);

    fread(&b.size, sizeof(b.size), 1, fp);
    fread(&b.type, sizeof(b.type), 1, fp);

    b.size = SERIALIZE32_LE(b.size);
    b.type = SERIALIZE32_LE(b.type);

    if (b.size == 1)
    {
        fread(&b.large_size, sizeof(b.large_size), 1, fp);
        b.large_size = SERIALIZE64_LE(b.large_size);
    }

    return b;
}

static Mp4FullBox mp4_parse_fb(Mp4Box b, Mp4FullBox fb, FILE *fp)
{
    memcpy(&fb, &b, sizeof(b));

    fread(&fb.version, sizeof(fb.version), 1, fp);
    fread(fb.flags, sizeof(*fb.flags), 3, fp);

    return fb;
}

static inline void mp4_seek_to_next_box(Mp4Box b, FILE *fp)
{
    fsetpos(fp, &gmi_box_fpos);
    fseeko(fp, (b.large_size) ? b.large_size : b.size, SEEK_CUR);
}

static Mp4FileTypeBox mp4_parse_ftyp(Mp4Box b_ftyp,
                                     Mp4FileTypeBox ftyp,
                                     FILE *fp)
{

    fread(&ftyp.major_brand, sizeof(ftyp.major_brand), 1, fp);
    ftyp.major_brand = SERIALIZE32_LE(ftyp.major_brand);

    get_raw_container(ftyp.major_brand, GMI_FTAG_MP4);

    mp4_seek_to_next_box(b_ftyp, fp);

    return ftyp;
}

static Mp4TimeToSampleBox mp4_parse_stts(Mp4FullBox fb_stts,
                                        Mp4TimeToSampleBox stts,
                                        FILE *fp)
{
    Mp4Box b_tmp = {};

    fread(&stts.entry_count, sizeof(stts.entry_count), 1, fp);
    fread(&stts.sample_count, sizeof(stts.sample_count), 1, fp);
    fread(&stts.sample_delta, sizeof(stts.sample_delta), 1, fp);

    stts.entry_count = SERIALIZE32_LE(stts.entry_count);
    stts.sample_count = SERIALIZE32_LE(stts.sample_count);
    stts.sample_delta = SERIALIZE32_LE(stts.sample_delta);

    get_raw_framerate(stts.sample_delta, GMI_FTYPE_DENOMINATOR);

    memcpy(&b_tmp, &fb_stts, sizeof(b_tmp));
    mp4_seek_to_next_box(b_tmp, fp);

    return stts;
}

static inline Mp4SampleEntry mp4_parse_se(Mp4SampleEntry se, FILE *fp)
{
    fseeko(fp, 8, SEEK_CUR); /* Skip some junk. */
    fread(&se.format, sizeof(se.format), 1, fp);
    fseeko(fp, 8, SEEK_CUR); /* Skip more junk. */

    return se;
}

static Mp4VisualSampleEntry mp4_parse_vse(Mp4VisualSampleEntry vse, FILE *fp)
{
    fseeko(fp, 16, SEEK_CUR); /* Skip some junk. */
    fread(&vse.width, sizeof(vse.width), 1, fp);
    fread(&vse.height, sizeof(vse.height), 1, fp);

    vse.width = SERIALIZE16_LE(vse.width);
    vse.height = SERIALIZE16_LE(vse.height);

    get_raw_resolution((size_t)vse.width, GMI_RTYPE_WIDTH);
    get_raw_resolution((size_t)vse.height, GMI_RTYPE_HEIGHT);

    return vse;
}

static Mp4SampleDescriptionBox mp4_parse_stsd(Mp4FullBox fb_stsd,
                                              Mp4SampleDescriptionBox stsd,
                                              uint32_t handler_type,
                                              FILE *fp)
{
    Mp4Box b_tmp = {};

    enum Mp4HdlrType {VIDE = 0x76696465, SOUN = 0x736f756e};

    switch (handler_type)
    {
        case VIDE:
        {
            stsd.se_vse = mp4_parse_se(stsd.se_vse, fp);
            get_raw_encoder(stsd.se_vse.format, GMI_ETYPE_VIDEO);
            stsd.vse = mp4_parse_vse(stsd.vse, fp);
            break;
        }
        case SOUN:
        {
            stsd.se_ase = mp4_parse_se(stsd.se_ase, fp);
            get_raw_encoder(stsd.se_ase.format, GMI_ETYPE_AUDIO);
            break;
        }
        default:
        {
            fputs("[Error] Parsed a trak without finding its hdlr first!\n", stderr);
            exit(1);
        }
    }

    memcpy(&b_tmp, &fb_stsd, sizeof(b_tmp));
    mp4_seek_to_next_box(b_tmp, fp);

    return stsd;
}

static Mp4SampleTableBox mp4_parse_stbl(Mp4SampleTableBox stbl,
                                        uint32_t handler_type,
                                        FILE *fp)
{
    enum Mp4BoxType {STSD = 0x73747364, STTS = 0x73747473};
    Mp4Box b_tmp = {};
    unsigned boxes_remaining = 2;

    while (mi.remaining && boxes_remaining)
    {
        b_tmp = mp4_parse_b(b_tmp, fp);
        switch (b_tmp.type)
        {
            case STSD:
            {
                stbl.fb_stsd = mp4_parse_fb(b_tmp, stbl.fb_stsd, fp);
                stbl.stsd = mp4_parse_stsd(stbl.fb_stsd, stbl.stsd, handler_type, fp);
                --boxes_remaining;
                break;
            }
            case STTS:
            {
                if (boxes_remaining == 1 && handler_type == 0x736f756e /* soun */)
                {
                    return stbl;
                }
                stbl.fb_stts = mp4_parse_fb(b_tmp, stbl.fb_stts, fp);
                stbl.stts = mp4_parse_stts(stbl.fb_stts, stbl.stts, fp);
                --boxes_remaining;
                break;
            }
            default:
            {
                mp4_seek_to_next_box(b_tmp, fp);
            }
        }
    }

    return stbl;
}

static Mp4MediaInformationBox mp4_parse_minf(Mp4MediaInformationBox minf,
                                             uint32_t handler_type,
                                             FILE *fp)
{
    enum Mp4BoxType {STBL = 0x7374626c};
    Mp4Box b_tmp = {};

    while (mi.remaining)
    {
        b_tmp = mp4_parse_b(b_tmp, fp);
        if (b_tmp.type == STBL)
        {
            minf.b_stbl = b_tmp;
            minf.stbl = mp4_parse_stbl(minf.stbl, handler_type, fp);
            break;
        }
        else
        {
            mp4_seek_to_next_box(b_tmp, fp);
        }
    }

    return minf;
}

static Mp4HandlerReferenceBox mp4_parse_hdlr(Mp4FullBox fb_hdlr,
                                             Mp4HandlerReferenceBox hdlr,
                                             FILE *fp)
{
    Mp4Box b_tmp = {};

    fseeko(fp, 4, SEEK_CUR); /* Skip some junk. */
    fread(&hdlr.handler_type, sizeof(hdlr.handler_type), 1, fp);

    hdlr.handler_type = SERIALIZE32_LE(hdlr.handler_type);
    if (mp4_bad_trak(hdlr.handler_type))
    {
        return hdlr;
    }

    memcpy(&b_tmp, &fb_hdlr, sizeof(b_tmp));
    mp4_seek_to_next_box(b_tmp, fp);

    return hdlr;
}

/*
 * TODO: This code duplication is silly. Maybe I'll do something about it later.
 */
static Mp4MediaHeaderBox32 mp4_parse_mdhd32(Mp4FullBox fb_mdhd,
                                            Mp4MediaHeaderBox32 mdhd32,
                                            uint32_t handler_type,
                                            FILE *fp)
{
    Mp4Box b_tmp = {};

    fseeko(fp, 8, SEEK_CUR); /* Skip some junk. */
    fread(&mdhd32.timescale, sizeof(mdhd32.timescale), 1, fp);

    mdhd32.timescale = SERIALIZE32_LE(mdhd32.timescale);
    if (handler_type == 0x76696465 /* vide */)
    {
        get_raw_framerate(mdhd32.timescale, GMI_FTYPE_NUMERATOR);
    }

    memcpy(&b_tmp, &fb_mdhd, sizeof(b_tmp));
    mp4_seek_to_next_box(b_tmp, fp);

    return mdhd32;
}

static Mp4MediaHeaderBox64 mp4_parse_mdhd64(Mp4FullBox fb_mdhd,
                                            Mp4MediaHeaderBox64 mdhd64,
                                            uint32_t handler_type,
                                            FILE *fp)
{
    Mp4Box b_tmp = {};

    fseeko(fp, 8, SEEK_CUR); /* Skip some junk. */
    fread(&mdhd64.timescale, sizeof(mdhd64.timescale), 1, fp);

    mdhd64.timescale = SERIALIZE32_LE(mdhd64.timescale);
    if (handler_type == 0x76696465 /* vide */)
    {
        get_raw_framerate(mdhd64.timescale, GMI_FTYPE_NUMERATOR);
    }

    memcpy(&b_tmp, &fb_mdhd, sizeof(b_tmp));
    mp4_seek_to_next_box(b_tmp, fp);

    return mdhd64;
}

static Mp4MediaBox mp4_parse_mdia(Mp4MediaBox mdia, FILE *fp)
{
    /*
     * I'm operating under the assumption that hdlr always appears before minf.
     * If that proves false, than we might end up doing more work than we need
     * to on a bad trak.
     */
    enum Mp4BoxType {MDHD = 0x6d646864, HDLR = 0x68646c72, MINF = 0x6d696e66};
    Mp4Box b_tmp  = {};
    bool premature_mdhd = false; /* In case we find mdhd before hdlr. */

    while (mi.remaining)
    {
        b_tmp = mp4_parse_b(b_tmp, fp);
        switch (b_tmp.type)
        {
            case MDHD:
            {
                if (!mdia.hdlr.handler_type)
                {
                    premature_mdhd = true;
                    gmi_mdhd_fpos = gmi_box_fpos;
                    mp4_seek_to_next_box(b_tmp, fp);
                    break;
                }
                mdia.fb_mdhd = mp4_parse_fb(b_tmp, mdia.fb_mdhd, fp);
                if (!mdia.fb_mdhd.version)
                {
                    mdia.mdhd32 = mp4_parse_mdhd32(mdia.fb_mdhd, mdia.mdhd32, mdia.hdlr.handler_type, fp);
                }
                else
                {
                    mdia.mdhd64 = mp4_parse_mdhd64(mdia.fb_mdhd, mdia.mdhd64, mdia.hdlr.handler_type, fp);
                }
                break;
            }
            case HDLR:
            {
                if (mdia.fb_hdlr.size)
                {
                    mp4_seek_to_next_box(b_tmp, fp);
                    break;
                }
                mdia.fb_hdlr = mp4_parse_fb(b_tmp, mdia.fb_hdlr, fp);
                mdia.hdlr = mp4_parse_hdlr(mdia.fb_hdlr, mdia.hdlr, fp);
                if (premature_mdhd)
                {
                    premature_mdhd = false;
                    fsetpos(fp, &gmi_mdhd_fpos);
                }
                if (gmi_bad_trak)
                {
                    return mdia;
                }
                break;
            }
            case MINF:
            {
                mdia.b_minf = b_tmp;
                mdia.minf = mp4_parse_minf(mdia.minf, mdia.hdlr.handler_type, fp);
                return mdia;
            }
            default:
            {
                mp4_seek_to_next_box(b_tmp, fp);
                break;
            }
        }
    }

    return mdia;
}

static Mp4TrackBox mp4_parse_trak(Mp4TrackBox trak, FILE *fp)
{
    enum Mp4BoxType {MDIA = 0x6d646961};
    Mp4Box b_tmp = {};

    while (mi.remaining)
    {
        b_tmp = mp4_parse_b(b_tmp, fp);
        if (b_tmp.type == MDIA)
        {
            trak.b_mdia = b_tmp;
            trak.mdia = mp4_parse_mdia(trak.mdia, fp);
            break;
        }
        else
        {
            mp4_seek_to_next_box(b_tmp, fp);
        }
    }

    return trak;
}

/*
 * TODO: This code duplication is silly. Maybe I'll do something about it later.
 */
static Mp4MovieHeaderBox32 mp4_parse_mvhd32(Mp4FullBox fb_mvhd,
                                            Mp4MovieHeaderBox32 mvhd32,
                                            FILE *fp)
{
    Mp4Box b_tmp = {};

    fread(&mvhd32.creation_time, sizeof(mvhd32.creation_time), 1, fp);
    fread(&mvhd32.modification_time, sizeof(mvhd32.modification_time), 1, fp);
    fread(&mvhd32.timescale, sizeof(mvhd32.timescale), 1, fp);
    fread(&mvhd32.duration, sizeof(mvhd32.duration), 1, fp);

    mvhd32.creation_time = SERIALIZE32_LE(mvhd32.creation_time);
    mvhd32.modification_time = SERIALIZE32_LE(mvhd32.modification_time);
    mvhd32.timescale = SERIALIZE32_LE(mvhd32.timescale);
    mvhd32.duration = SERIALIZE32_LE(mvhd32.duration);

    get_raw_timestamp((size_t)mvhd32.creation_time, GMI_TTYPE_CREATION, GMI_FTAG_MP4);
    get_raw_timestamp((size_t)mvhd32.modification_time, GMI_TTYPE_MODIFICATION, GMI_FTAG_MP4);
    get_raw_duration(mvhd32.timescale, GMI_DTYPE_TIMEBASE);
    get_raw_duration((size_t)mvhd32.duration, GMI_DTYPE_DURATION);

    memcpy(&b_tmp, &fb_mvhd, sizeof(b_tmp));
    mp4_seek_to_next_box(b_tmp, fp);

    return mvhd32;
}

static Mp4MovieHeaderBox64 mp4_parse_mvhd64(Mp4FullBox fb_mvhd,
                                            Mp4MovieHeaderBox64 mvhd64,
                                            FILE *fp)
{
    Mp4Box b_tmp = {};

    fread(&mvhd64.creation_time, sizeof(mvhd64.creation_time), 1, fp);
    fread(&mvhd64.modification_time, sizeof(mvhd64.modification_time), 1, fp);
    fread(&mvhd64.timescale, sizeof(mvhd64.timescale), 1, fp);
    fread(&mvhd64.duration, sizeof(mvhd64.duration), 1, fp);

    mvhd64.creation_time = SERIALIZE64_LE(mvhd64.creation_time);
    mvhd64.modification_time = SERIALIZE64_LE(mvhd64.modification_time);
    mvhd64.timescale = SERIALIZE32_LE(mvhd64.timescale);
    mvhd64.duration = SERIALIZE64_LE(mvhd64.duration);

    get_raw_timestamp(mvhd64.creation_time, GMI_TTYPE_CREATION, GMI_FTAG_MP4);
    get_raw_timestamp(mvhd64.modification_time, GMI_TTYPE_MODIFICATION, GMI_FTAG_MP4);
    get_raw_duration(mvhd64.timescale, GMI_DTYPE_TIMEBASE);
    get_raw_duration(mvhd64.duration, GMI_DTYPE_DURATION);

    memcpy(&b_tmp, &fb_mvhd, sizeof(b_tmp));
    mp4_seek_to_next_box(b_tmp, fp);

    return mvhd64;
}

static Mp4MovieBox mp4_parse_moov(Mp4Box b_moov, Mp4MovieBox moov, FILE *fp)
{
    enum Mp4BoxType {MVHD = 0x6d766864, TRAK = 0x7472616b};
    Mp4Box     b_tmp  = {};
    unsigned   trak_idx = 0;
    fpos_t     moov_start = gmi_box_fpos;

    while(mi.remaining)
    {
        b_tmp = mp4_parse_b(b_tmp, fp);
        switch (b_tmp.type)
        {
            case MVHD:
            {
                moov.fb_mvhd = mp4_parse_fb(b_tmp, moov.fb_mvhd, fp);
                if (!moov.fb_mvhd.version)
                {
                    moov.mvhd32 = mp4_parse_mvhd32(moov.fb_mvhd, moov.mvhd32, fp);
                }
                else
                {
                    moov.mvhd64 = mp4_parse_mvhd64(moov.fb_mvhd, moov.mvhd64, fp);
                }
                break;
            }
            case TRAK:
            {
                gmi_trak_fpos = gmi_box_fpos;
                moov.b_trak = b_tmp;
                moov.trak[trak_idx] = mp4_parse_trak(moov.trak[trak_idx], fp);
                if (gmi_bad_trak)
                {
                    /* Let's try again. */
                    gmi_bad_trak = false;
                }
                else
                {
                    ++gmi_trak_success;
                    ++trak_idx;
                }
                gmi_box_fpos = gmi_trak_fpos;
                mp4_seek_to_next_box(b_tmp, fp);
                break;
            }
            default:
            {
                mp4_seek_to_next_box(b_tmp, fp);
                break;
            }
        }

        if (mi.remaining && (ftello(fp) >= moov_start + b_moov.size))
        {
            fprintf(stdout, "[Notice] Unexpected end of moov box. %u trak(s) successful.\n", gmi_trak_success);
            break;
        }
    }

    return moov;
}

Mp4MinSpec mp4_parse(Mp4MinSpec mp4, FILE *fp)
{
    enum Mp4BoxType {MOOV = 0x6d6f6f76};
    Mp4Box b_tmp = {};

    mp4.ftyp = mp4_parse_ftyp(mp4.b_ftyp, mp4.ftyp, fp);

    while(mi.remaining)
    {
        b_tmp = mp4_parse_b(b_tmp, fp);
        if (b_tmp.type == MOOV)
        {
            mp4.b_moov = b_tmp;
            mp4.moov = mp4_parse_moov(mp4.b_moov, mp4.moov, fp);
            break;
        }
        else
        {
            mp4_seek_to_next_box(b_tmp, fp);
        }
    }

    return mp4;
}

#ifdef CHEXT_DEBUG
void report_raw_mediainfo(void)
{
    const char fmtmsg_dbg_mi_raw[] =
    {
        "[Debug] Raw Mediainfo Dump\n"
        "                     TYPE            VALUE UNITS\n"
        "                 filesize %16"PRIu64" bytes\n"
        "                container %16s\n"
        "            time.creation %16"PRIu64" UNIX timestamp\n"
        "        time.modification %16"PRIu64" UNIX timestamp\n"
        "             dur.timebase %16"PRIu32" tics\n"
        "             dur.duration %16"PRIu64" timebase tics\n"
        "                  fps.num %16"PRIu32"\n"
        "                  fps.den %16"PRIu32"\n"
        "                res.width %16"PRIu16" px\n"
        "               res.height %16"PRIu16" px\n"
        "                enc.video %16s\n"
        "                enc.audio %16s\n\n"
    };

    char enc_video_str[5] = {};
    char enc_audio_str[5] = {};

    /* Stringify these char arrays. */
    memcpy(enc_video_str, mi.raw.enc.video, sizeof(mi.raw.enc.video));
    memcpy(enc_audio_str, mi.raw.enc.audio, sizeof(mi.raw.enc.audio));

    fprintf(stdout, fmtmsg_dbg_mi_raw, mi.raw.filesize, mi.raw.container,
            mi.raw.time.creation, mi.raw.time.modification, mi.raw.dur.timebase,
            mi.raw.dur.duration, mi.raw.fps.num, mi.raw.fps.den,
            mi.raw.res.width, mi.raw.res.height, enc_video_str, enc_audio_str);
}
#endif

void get_mediainfo(FILE *fp)
{
    static_assert(_FILE_OFFSET_BITS == 64, "_FILE_OFFSET_BITS=64 macro must be defined!");

    get_raw_filesize(mi.raw.filesize, fp);
    Mp4MinSpec mp4 = {};

    mp4.b_ftyp = mp4_parse_b(mp4.b_ftyp, fp);
    if (mp4.b_ftyp.type == 0x66747970 /* ftyp */)
    {
        mp4 = mp4_parse(mp4, fp);
    }
    else
    {
        fputs("[Warning] Only mp4 mediainfo extraction is currently supported!\n", stderr);
        rewind(fp);
    }

#ifdef CHEXT_DEBUG
    report_raw_mediainfo();
#endif
    //report_missing_mediainfo();
}

#ifdef CHEXT_TEST_GET_MEDIAINFO
int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fputs("[Error] Give me a video to test!\n", stderr);
        return 1;
    }

    FILE *fp = fopen(argv[1], "rb");
    if (!fp)
    {
        fprintf(stderr, "[Error] Couldn't open video file: %s\n", argv[1]);
        return 1;
    }

    get_mediainfo(fp);

    return 0;
}
#endif

#undef fseek
#undef ftell

#ifdef __cplusplus
}
#endif
