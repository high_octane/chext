/*
 * Copyright 2019 high_octane
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * metadata      | location in mp4
 * --------------+----------------
 * container     | ftyp
 * creation_time | moov.mvhd
 * fps.num       | moov.trak["vide"].mdia.mdhd
 * fps.den       | moov.trak["vide"].mdia.minf.stbl.stts
 * duration      | moov.mvhd
 * resolution    | moov.trak["vide"].mdia.minf.stbl.stsd
 * video_encoder | moov.trak["vide"].mdia.minf.stbl.stsd
 * audio_encoder | moov.trak["soun"].mdia.minf.stbl.stsd
 */

/*
 * Box and FullBox contain metadata pertaining to the box types themselves.
 */
typedef struct
{
    uint32_t size;
    uint32_t type;       /* AKA header.*/
    uint64_t large_size; /* If size=1, then large_size contains 64-bit size */
} Mp4Box;

typedef struct
{
    uint32_t size;
    uint32_t type;    /* AKA header.*/
    uint64_t large_size; /* If size=1, then large_size contains 64-bit size. */
    uint8_t  version;    /* If version=1, then use 64-bit box type variant. */
    uint8_t flags[3];
} Mp4FullBox;

/*
 * Box Type:     ‘stts’
 * Container:    Media Information Box (‘stbl’)
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    uint32_t entry_count;
    uint32_t sample_count;
    uint32_t sample_delta; /* Fps denominator. */
} Mp4TimeToSampleBox;

typedef struct
{
    /* Skip 8 bytes */
    uint32_t format; /* This will tell us the encoder's name. */
    /* Skip 8 bytes. */
} Mp4SampleEntry;

typedef struct
{
    /* Skip 16 bytes. */
    uint16_t width;  /* In pixels. */
    uint16_t height; /* In pixels. */
    /* Skip the rest. */
} Mp4VisualSampleEntry;

/*
 * Box Type:     ‘stsd’
 * Container:    Media Information Box (‘stbl’)
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    uint32_t entry_count;
    union
    {
        /* vide */
        struct
        {
            Mp4SampleEntry       se_vse;
            Mp4VisualSampleEntry vse;
        };
        /* soun */
        Mp4SampleEntry se_ase;
    };
} Mp4SampleDescriptionBox;

/*
 * Box Type:     ‘stbl’
 * Container:    Media Information Box (‘minf’)
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    Mp4FullBox              fb_stsd;
    Mp4SampleDescriptionBox stsd;
    Mp4FullBox              fb_stts;
    Mp4TimeToSampleBox      stts;
} Mp4SampleTableBox;

/*
 * Box Type:     ‘minf’
 * Container:    Media Box (‘mdia’)
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    Mp4Box            b_stbl;
    Mp4SampleTableBox stbl;
} Mp4MediaInformationBox;

/*
 * Box Type:     ‘hdlr’
 * Container:    Media Box (‘mdia’) or Meta Box (‘meta’)
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    /* Skip 4 bytes. */
    uint32_t handler_type; /* Determines trak type. Value = "vide" or "soun". */
    /* Skip the rest. */
} Mp4HandlerReferenceBox;

/*
 * Box Type:     ‘mdhd’
 * Container:    Media Box (‘mdhd’)
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    /* Skip 8 bytes. */
    uint32_t timescale; /* Fps Numerator. */
    /* Skip the rest. */

} Mp4MediaHeaderBox32;

typedef struct
{
    /* Skip 8 bytes. */
    uint32_t timescale; /* Fps Numerator. */
    /* Skip the rest. */

} Mp4MediaHeaderBox64;

/*
 * Box Type:     ‘mdia’
 * Container:    Track Box (‘trak’)
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    Mp4FullBox             fb_mdhd;
    union
    {
        Mp4MediaHeaderBox32 mdhd32;
        Mp4MediaHeaderBox64 mdhd64;
    };
    Mp4FullBox             fb_hdlr;
    Mp4HandlerReferenceBox hdlr;
    Mp4Box                 b_minf;
    Mp4MediaInformationBox minf;
} Mp4MediaBox;

/*
 * Box Type:     ‘trak’
 * Container:    Movie Box (‘moov’)
 * Mandatory:    Yes
 * Quantity:     One or more
 */
typedef struct
{
    Mp4Box      b_mdia;
    Mp4MediaBox mdia;
} Mp4TrackBox;
/*
 * Box Type:     ‘mvhd’
 * Container:    Movie Box (‘moov’)
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    uint32_t  creation_time;
    uint32_t  modification_time;
    uint32_t  timescale;
    uint32_t  duration;
    /* Skip the rest. */
} Mp4MovieHeaderBox32;

typedef struct
{
    uint64_t  creation_time;
    uint64_t  modification_time;
    uint32_t  timescale;
    uint64_t  duration;
    /* Skip the rest. */
} Mp4MovieHeaderBox64;

/*
 * Box Type:     ‘moov’
 * Container:    File
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    Mp4FullBox  fb_mvhd;
    union
    {
        Mp4MovieHeaderBox32 mvhd32;
        Mp4MovieHeaderBox64 mvhd64;
    };
    Mp4Box      b_trak;
    Mp4TrackBox trak[3]; /* Only looking for audio and video tracks. Allocate
                            one more for dummy trak. */
} Mp4MovieBox;

/*
 * Box Type:     `ftyp’
 * Container:    File
 * Mandatory:    Yes
 * Quantity:     Exactly one
 */
typedef struct
{
    uint32_t major_brand; /* We'll use this the get the "flavor" of mp4. */
    /* Skip the rest. */
} Mp4FileTypeBox;


/*
 * The minimal mp4 specification required to extract the metadata that I'm
 * interested in.
 */
typedef struct
{
    Mp4Box         b_ftyp;
    Mp4FileTypeBox ftyp;
    Mp4Box         b_moov;
    Mp4MovieBox    moov;
} Mp4MinSpec;
