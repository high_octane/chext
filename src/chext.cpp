/*
 * Copyright 2019 high_octane
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include "serialize.h"
#include "mp4_minspec.h"
#include "get_mediainfo.c"
#include "opencv2/opencv.hpp"

#define DS_BUF_SIZE 0x40u

# define DS_PUTTEXT \
    putText(frame, cursor, Point(10, 30 + y_dist), \
            FONT_HERSHEY_DUPLEX, 1.0, Scalar(255, 0, 255), 1)

using namespace std;
using namespace cv;

enum IOErrorType {IO_ERROR_FILE, IO_MISSING_ARGUMENT};
static const char *fmtmsg_io_errors[] =
{
    {
        "[Error] Couldn't open file: %s\n"
    },
    {
        "[Error] No file was specified.\n"
    }
};

[[noreturn]] static void io_error(const char *filename, enum IOErrorType et)
{
    fprintf(stderr, fmtmsg_io_errors[et], filename);
    exit(1);
}

typedef struct
{
    int  update_ms;
    int  toggle_update_ms[2];
    bool stats_state;
    bool toggle_stats_state[2];
} InputData;

typedef struct
{
    ssize_t ticks_init;
    ssize_t ticks_elapsed;
    double  mspf;
    double  fps;
} TimingData;

static inline double get_end_pos(VideoCapture vc)
{
    double end_pos = 0.0;

    vc.set(CAP_PROP_POS_AVI_RATIO, 1.0);
    end_pos = vc.get(CAP_PROP_POS_MSEC);
    vc.set(CAP_PROP_POS_AVI_RATIO, 0.0);

    return end_pos / 1000.0;
}

static void disp_stats(TimingData td, double cur_pos, double end_pos, Mat& frame)
{
    const char *fmt = "Pos: %.2f / %.2f\nFps: %.2f\nMs/f: %.2f\n";
    char buf[DS_BUF_SIZE] = {};

    snprintf(buf, DS_BUF_SIZE, fmt, cur_pos, end_pos, td.fps, td.mspf);

    char *cursor = buf;
    char *end = NULL;
    for (int y_dist = 0; DS_BUF_SIZE > cursor - buf && cursor[0] != '\0'; y_dist += 30)
    {
        end = (char *)memchr(cursor, '\n', strlen(cursor));

        if(end == NULL)
        {
            DS_PUTTEXT;
            break;
        }

        *end = '\0';
        DS_PUTTEXT;

        cursor = end + 1;
    }
}

static void handle_input(InputData *id, VideoCapture *vc)
{
    // waitKey seems wonky to me. Maybe there's a better way to process input.
    switch(waitKey(id->update_ms))
    {
        case '.':
        {
            vc->set(CAP_PROP_POS_MSEC, vc->get(CAP_PROP_POS_MSEC) + 5000.0);
            break;
        }
        case ',':
        {
            vc->set(CAP_PROP_POS_MSEC, vc->get(CAP_PROP_POS_MSEC) - 5000.0);
            break;
        }
        case ' ':
        case 'p':
        {
            id->update_ms = id->toggle_update_ms[id->update_ms];
            break;
        }
        case 's':
        {
            id->stats_state = id->toggle_stats_state[id->stats_state];
            break;
        }
        case 'q':
        {
            exit(0);
            break;
        }
    }
}

static void report_keyboard_mapping(void)
{
    const char kb_map[] =
    {
        "[Notice] Default Keyboard Map\n"
        "         KEY       DESCRIPTION\n"
        "         .         Move 5 secs forward.\n"
        "         ,         Move 5 secs backward.\n"
        "         <space> p Toggle pause state of frame processor.\n"
        "         s         Toggle display of stats in the upper-left corner.\n"
        "         q         Quit.\n"
    };

    fputs(kb_map, stdout);
}


int main(int argc, char **argv)
{
    if (argc < 2)
    {
        io_error(argv[1], IO_MISSING_ARGUMENT);
    }

    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

    FILE *fp = fopen(argv[1], "rb");
    if (!fp)
    {
        io_error(argv[1], IO_ERROR_FILE);
    }
    get_mediainfo(fp);
    fclose(fp);

    VideoCapture vc(argv[1]);
    if (!vc.isOpened())
    {
        io_error(argv[1], IO_ERROR_FILE);
    }

    double end_pos = get_end_pos(vc);

    TimingData td = {};
#if CHEXT_DEBUG
    InputData  id = {1, {1, 0}, true, {true, false}};
#else
    InputData  id = {1, {1, 0}, false, {true, false}};
#endif
    namedWindow("Test", WINDOW_AUTOSIZE);
    Mat frame = {};

    report_keyboard_mapping();

    // Performance is not ideal right now. Parallelization is required to
    // achieve faster video processing speeds.
    while (true)
    {
        td.ticks_init = getTickCount();
        if(!vc.read(frame))
        {
            break;
        }

        handle_input(&id, &vc);

        td.ticks_elapsed = getTickCount() - td.ticks_init;
        if (id.stats_state)
        {
            td.mspf = ((double)td.ticks_elapsed / getTickFrequency()) * 1000.0;
            td.fps = getTickFrequency() / (double)td.ticks_elapsed;
            disp_stats(td, vc.get(CAP_PROP_POS_MSEC) / 1000.0, end_pos, frame);
        }

        imshow("Test", frame);
    }

    return 0;
}
