/*
 * Copyright 2019 high_octane
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BSWAP16(x)            \
    ((((x) & 0xFF00u) >> 8) | \
     (((x) & 0x00FFu) << 8))

#define BSWAP32(x)                 \
    ((((x) & 0xFF000000u) >> 24) | \
     (((x) & 0x00FF0000u) >> 8)  | \
     (((x) & 0x0000FF00u) << 8)  | \
     (((x) & 0x000000FFu) << 24))

#define BSWAP64(x)                           \
    ((((x) & 0xFF00000000000000ull) >> 56) | \
     (((x) & 0x00FF000000000000ull) >> 40) | \
     (((x) & 0x0000FF0000000000ull) >> 24) | \
     (((x) & 0x000000FF00000000ull) >> 8)  | \
     (((x) & 0x00000000FF000000ull) << 8)  | \
     (((x) & 0x0000000000FF0000ull) << 24) | \
     (((x) & 0x000000000000FF00ull) << 40) | \
     (((x) & 0x00000000000000FFull) << 56))


#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__

/* Convert to little-endian from big-endian. */
#define SERIALIZE16_LE(x) BSWAP16(x)
#define SERIALIZE32_LE(x) BSWAP32(x)
#define SERIALIZE64_LE(x) BSWAP64(x)

#else

#define SERIALIZE16_LE(x) (x)
#define SERIALIZE32_LE(x) (x)
#define SERIALIZE64_LE(x) (x)

#endif

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__

/* Convert to big-endian from little-endian. */
#define SERIALIZE16_BE(x) BSWAP16(x)
#define SERIALIZE32_BE(x) BSWAP32(x)
#define SERIALIZE64_BE(x) BSWAP64(x)

#else

#define SERIALIZE16_BE(x) (x)
#define SERIALIZE32_BE(x) (x)
#define SERIALIZE64_BE(x) (x)

#endif
